# TODOs

- Multithreading
- Include integration tests in Sonar analysis
- Compare implementation with https://www.bigocheatsheet.com/
- Use functional approach, rewrite the algorithm implementations
- jdk17 update
