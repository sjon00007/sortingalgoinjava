# Exercises and papers

## Exercises

- Count Sort, Bucket Sort, Radix Sort (Non-Comparison Sorting)
  - Reference: CSE 2320 – Algorithms and Data Structures / University of Texas at Arlington

<object data="exercises/count-bucket-radix-sort.pdf" type="application/pdf" width="700px" height="700px">
    <embed src="exercises/count-bucket-radix-sort.pdf">
        <p>This browser does not support PDFs. Please download the PDF to view it: <a href="exercises/count-bucket-radix-sort.pdf">Download PDF</a>.</p>
    </embed>
</object>

- Counting sort

![img](exercises/counting-sort-1.png)

## Papers

- A comparative Study of Sorting Algorithms / Comb, Cocktail and Counting Sorting
  - Reference: International Research Journal of Engineering and Technology (IRJET)
  - Date: 04 Issue: 01 | Jan‐2017

<object data="exercises/irjet-v4i1249-comparative-study-of-sorting-algorithms.pdf" type="application/pdf" width="700px" height="700px">
    <embed src="exercises/count-bucket-radix-sort.pdf">
        <p>This browser does not support PDFs. Please download the PDF to view it: <a href="exercises/irjet-v4i1249-comparative-study-of-sorting-algorithms.pdf">Download PDF</a>.</p>
    </embed>
</object>
